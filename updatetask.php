<?php
	require_once '/handlers/loginHandler.php';
	
	require_once '/db_files/config.php';
	require_once '/db_files/database.php';
	require_once '/handlers/userHandler.php';
	require_once '/handlers/taskHandler.php';
?>

<?php
	$code = "";
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		
		$code = trim($_POST["taskcode"]);
		$taskname = trim($_POST["taskname"]);
		$description = trim($_POST["description"]);
		$keepFile = isset($_POST["keepfile"]);
		$message;
		$type;
		
				
		if(!empty($code) && !empty($taskname)){
			if($keepFile){
				$link = open_database();
				$result = task_update($link, $code, $taskname, $description);
				close_database($link);
				if($result[0] == "error"){
					$type = "update_taskerror";
					$message = $result[1];
				}else{
					$type = "update_tasksuccess";
					$message = $result[1];
				}
			}else{
				if($_FILES["attachment"]["error"] == 0){
					$fileName = $_FILES['attachment']['name'];
					$fileType = $_FILES['attachment']['type'];
					
					$fileSize = $_FILES['attachment']['size'];
					$fileTemp = $_FILES['attachment']['tmp_name'];
					
					$fp = fopen($fileTemp, "r");
					$fileContent = fread($fp, $fileSize);
					fclose($fp);
					
					$link = open_database();
					$result = task_updatefile($link, $code, $taskname, $description, $fileContent, $fileName, $fileType);

					
					close_database($link);
					if($result[0] == "error"){
						$type = "update_taskerror";
						$message = $result[1];
					}else{
						$type = "update_tasksuccess";
						$message = $result[1];
					}
				}else{
					$type = "update_taskerror";
					$message = "Fields must be filled.";
				}
			}
		}else{
			$type = "update_taskerror";
			$message = "Fields must be filled.";
		}
		header("location: taskFormUpdate.php?$type=$message&taskcode=$code");
	}
?>