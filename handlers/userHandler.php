<?php

	mysqli_report(MYSQLI_REPORT_STRICT);
	
	function user_insert($link, $email, $password) {
		$messages = array(
			1 => "User already exists.",
			2 => "Something went wrong.",
			3 => "User successfully registered."
		);
		
		if(user_exists($link, $email)){
			return array("error", $messages[1]); 
		}else{
			$sql = "INSERT INTO users(user_email, user_password) VALUES (?, ?)";
			if($stmt = $link->prepare($sql)){
				$stmt->bind_param("ss", $param_email, $param_password);
				$param_email = $email;
				$param_password = $password;
				if($stmt->execute()){
					$stmt->close();
					return array("success", $messages[3]);
				}else{
					return array("error", $messages[2]); 
				}
			}else{
				return array("error", $messages[2]);
			}
		}
		
	}

	function user_exists($link, $email) {
		if(empty(trim($email))){
			return 0;
		}else{
			$sql = "SELECT * FROM users WHERE user_email = ?";
			if($stmt = $link->prepare($sql)){
				$stmt->bind_param("s", $param_email);
				$param_email = $email;
				if($stmt->execute()){
					$stmt->store_result();
					if($stmt->num_rows == 0){
						$stmt->close();
						return 0;
					}else{
						$stmt->close();
						return 1;
					}
				}
			}else{
				return 0;
			}
		}
	}
	
	function user_check_credentials($link, $email, $password) {
		if(empty(trim($email))){
			return 0;
		}else{
			$sql = "SELECT * FROM users WHERE user_email = ? AND user_password = ?";
			if($stmt = $link->prepare($sql)){
				$stmt->bind_param("ss", $param_email, $param_password);
				$param_email = $email;
				$param_password = $password;
				if($stmt->execute()){
					$stmt->store_result();
					if($stmt->num_rows == 0){
						$stmt->close();
						return 0;
					}else{
						$stmt->close();
						return 1;
					}
				}
			}else{
				return 0;
			}
		}
	}
?>
