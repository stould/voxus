<?php
	require_once '/handlers/loginHandler.php';
	
	mysqli_report(MYSQLI_REPORT_STRICT);
	
	function task_insert($link, $code, $taskname, $description, $fileContent, $fileName, $fileType) {
		$messages = array(
			1 => "Task already exists.",
			2 => "Something went wrong.",
			3 => "Task successfully registered."
		);
		$sql = "INSERT INTO tasks(task_code, task_name, task_description, task_filedata, task_filename, task_filetype) VALUES (?, ?, ?, ?, ?, ?)";
		if($stmt = $link->prepare($sql)){
			$null = NULL;
			$stmt->bind_param("sssbss", $param_code, $param_taskname, $param_description, $null, $param_filename, $param_filetype);
			$param_code = $code;
			$param_taskname = $taskname;
			$param_description = $description;
			$stmt->send_long_data(3, $fileContent);
			$param_filename = $fileName;
			$param_filetype = $fileType;

			if($stmt->execute()){
				$stmt->close();
				return array("success", $messages[3]);
			}else{
				return array("error", $messages[1]); 
			}
		}else{
			return array("error", $messages[2]);
		}
	}
	
	function task_updatefile($link, $code, $taskname, $description, $fileContent, $fileName, $fileType) {
		$messages = array(
			1 => "Task already exists.",
			2 => "Something went wrong.",
			3 => "Task successfully updated."
		);
		$sql = "UPDATE tasks SET task_name = ?, task_description = ?, task_filedata = ?, task_filename = ?, task_filetype = ? WHERE task_code = ?";
		$stmt = $link->prepare($sql);
		if($stmt){
			$null = NULL;
			$stmt->bind_param("ssbsss", $param_taskname, $param_description, $null, $param_filename, $param_filetype, $param_taskcode);
			$param_taskname = $taskname;
			$param_description = $description;
			$stmt->send_long_data(2, $fileContent);
			$param_filename = $fileName;
			$param_filetype = $fileType;
			$param_taskcode = $code;
			

			if($stmt->execute()){
				$stmt->close();
				return array("success", $messages[3]);
			}else{
				return array("error", $messages[1]); 
			}
		}else{

			return array("error", $messages[2]);
		}
	}
	
	function task_update($link, $code, $taskname, $description) {
		$messages = array(
			1 => "Task already exists.",
			2 => "Something went wrong.",
			3 => "Task successfully updated."
		);
		$sql = "UPDATE tasks SET task_name = ?, task_description = ? WHERE task_code = ?";
		if($stmt = $link->prepare($sql)){
			$null = NULL;
			$stmt->bind_param("sss", $param_taskname, $param_description, $param_taskcode);
			$param_taskname = $taskname;
			$param_description = $description;
			$param_taskcode = $code;
			if($stmt->execute()){
				$stmt->close();
				return array("success", $messages[3]);
			}else{
				return array("error", $messages[1]); 
			}
		}else{
			return array("error", $messages[2]);
		}
	}
	
	function task_delete($link, $code){
		$sql = "DELETE FROM tasks WHERE task_code = ?";
		if($stmt = $link->prepare($sql)){
			$stmt->bind_param("s", $param_code);
			$param_code = $code;
			if($stmt->execute()){
				
				if($stmt->affected_rows == 1){
					$stmt->close();
					return Array("success", "Task successfully deleted.");
				}else{
					$stmt->close();
					return array("error", "Code does not exist."); 
				}
			}else{
				return array("error", "Code does not exist."); 
			}
		}else{
			return array("error", "Something went wrong.");
		}
	}
	
	function task_getContent($link, $code){
		$sql = "SELECT task_filedata, task_filename, task_filetype FROM tasks WHERE task_code = ?";
		if($stmt = $link->prepare($sql)){
			$stmt->bind_param("s", $param_code);
			$param_code = $code;
			if($stmt->execute()){
				$result = $stmt->get_result()->fetch_assoc();
				$stmt->close();				
				return Array("success", ["name" => $result["task_filename"], "type" => $result["task_filetype"], "content" => $result["task_filedata"]]);		
			}else{
				return array("error", "Code does not exist."); 
			}
		}else{
			return array("error", "Something went wrong.");
		}
	}
	
	function task_downloadFile($file){
		header('Content-Disposition: attachment; filename="'.$file["name"].'"');
		print($file["content"]);
	}
	
	function task_getInRange($link, $from, $to){
		$sql = "SELECT task_code, task_name, task_description, task_filename FROM tasks ORDER BY task_code DESC LIMIT ?, ?	";
		if($stmt = $link->prepare($sql)){
			$stmt->bind_param("ii", $param_from, $param_to);
			$param_from = $from;
			$param_to = $to;
			if($stmt->execute()){
				$stmt->bind_result($code, $name, $desc, $fileName);
				$result = Array();
				while($stmt->fetch()){
					$row = Array(
						"code" => $code, 
						"name" => $name, 
						"desc" => $desc, 
						"fileName" => $fileName);
					array_push($result, $row);
				}
				
				$stmt->close();				
				return Array("success", $result);		
			}else{
				return array("error", "algo deu errado no STMT"); 
			}
		}else{
			return array("error", "algo deu errado no prepare");
		}
	}
	
	function task_getByCode($link, $code){
		$sql = "SELECT task_code, task_name, task_description, task_filename FROM tasks WHERE task_code = ?";
		if($stmt = $link->prepare($sql)){
			$stmt->bind_param("s", $param_code);
			$param_code = $code;
			if($stmt->execute()){
				$result = $stmt->get_result()->fetch_assoc();
				$stmt->close();				
				return Array("success", ["code" => $result["task_code"], "name" => $result["task_name"], "description" => $result["task_description"], "filename" => $result["task_filename"]]);		
			}else{
				return array("error", "Code does not exist."); 
			}
		}else{
			return array("error", "Something went wrong.");
		}
	}
	
	
	
	function task_count($link){
		$sql = "SELECT COUNT(*) as cnt FROM tasks";
		if($stmt = $link->prepare($sql)){
			if($stmt->execute()){
				$result = $stmt->get_result()->fetch_assoc();
				$stmt->close();
				return Array("success", $result["cnt"]);
			}else{
				return array("error", "Code does not exist."); 
			}
		}else{
			return array("error", "Something went wrong.");
		}
	}
?>
