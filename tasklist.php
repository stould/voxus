<?php
	require_once '/handlers/loginHandler.php';
	require_once '/db_files/config.php';
	require_once '/db_files/database.php';
	require_once '/handlers/taskHandler.php';
?>

<html>
	<head>
		<meta charset="UTF-8">
		<title>Task list</title>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

		<style type="text/css">
			table {
				table-layout: fixed;
				word-wrap: break-word;
			}
			.wrapper{ width: 400px; padding: 20px; }
		</style>
		
	</head>
	<body>
		<?php include("header.php") ?>
		
		<table class="table table-hover">
			<thead class="thead-light">
				<tr>
					<th scope="col"># Code</th>
					<th scope="col">Name</th>
					<th width="25%" scope="col">Description</th>
					<th scope="col">Download</th>
					<th scope="col">Actions</th>
					<th scope="col" >
						<a href="taskform.php" class="btn  btn-success glyphicon glyphicon-plus"> Add</a>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$results_per_page;
					
					if(ISSET($_GET["resultlimit"]) ){//&& is_integer($_GET["resultlimit"]
						$results_per_page = $_GET["resultlimit"];
						if($results_per_page == 0) $results_per_page++;
					}else{
						$results_per_page = 5;
					}
					
					
					
					$current_resultset;
					
					if(ISSET($_GET["resultfrom"]) ){//&& is_integer($_GET["resultfrom"]
						$current_resultset = $_GET["resultfrom"];
					}else{
						$current_resultset = 0;
					}
					
					$link = open_database();
					$totalTasks = task_count($link)[1];
					$tasks = task_getInRange($link, $current_resultset, $results_per_page);
					close_database($link);
					
					$n = count($tasks[1]);				
					
					for($i = 0; $i < $n; $i++){
						echo '<tr>';
							echo '<td scope="row">'.$tasks[1][$i]["code"].'</td>';
							echo '<td scope="row">'.$tasks[1][$i]["name"].'</td>';
							echo '<td scope="row">'.$tasks[1][$i]["desc"].'</td>';
							echo '<td scope="row">';
								echo '<a class="glyphicon glyphicon-download" href="/download.php?taskCode='.$tasks[1][$i]["code"].'"> '.substr($tasks[1][$i]["fileName"],0,15).'...</a>';
							echo '</td>';
							echo '<td scope="row">';
								echo '<a href="taskFormUpdate.php?taskcode='.$tasks[1][$i]["code"].'" class="btn btn-info btn-group" role="group">Edit</a>.';
								echo '<a onclick=\'return confirm("Do you really want to exclude task \"'.$tasks[1][$i]["code"].'\" ?");\' href="deletetask.php?taskcode='.$tasks[1][$i]["code"].'" class="btn btn-danger btn-group" role="group">Exclude</a>';
							echo '</td>';
							echo '<td></td>';
						echo '</tr>';
					}
				?>
		</table>
		<div class="container text-center">
			<ul class="pagination">
				<?php
					$pages = intval(intval($totalTasks) / intval($results_per_page)) + ($totalTasks % $results_per_page != 0);
					for($i = 0; $i < $pages; $i++){
						$currentURI = $_SERVER['PHP_SELF'].'?resultfrom='.($results_per_page * $i)."&resultlimit=".$results_per_page;
						if($i == $current_resultset/$results_per_page){
							echo '<li class="page-item active"><a class="page-link" href="'.$currentURI.'">'.($i + 1).'</a></li>';
						}else{
							echo '<li class="page-item"><a class="page-link" href="'.$currentURI.'">'.($i + 1).'</a></li>';
						}
					}
				?>
			</ul>
		</div>
	</body>
</html>