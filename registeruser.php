<?php
	require_once '/handlers/loginHandler.php';
	
	require_once '/db_files/config.php';
	require_once '/db_files/database.php';
	require_once '/handlers/userHandler.php';
?>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$email = trim($_POST["email"]);
		$password = trim($_POST["password"]);
		$password_confirm = trim($_POST["password_confirm"]);
		
		$type;
		$message;
		
		if($password != $password_confirm){
			$type = "register_usererror";
			$message = "Passoword diff from password confirmation.";
		}else if(empty($email) || empty($password) || empty($password_confirm)){
			$type = "register_usererror";
			$message = "Fields must be filled.";
		}else{
			$link = open_database();
			$result = user_insert($link, $email, $password);
			close_database($link);
			if($result[0] == "error"){
				$type = "register_usererror";
				$message = $result[1];
			}else{
				$type = "register_usersuccess";
				$message = $result[1];
			}
		}
		header("location: userform.php?$type=$message");
	}
?>