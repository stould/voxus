<?php

	mysqli_report(MYSQLI_REPORT_STRICT);

	function open_database() {
		try {
			$link = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
			return $link;
		} catch (Exception $e) {
			echo $e->getMessage();
			return null;
		}
	}

	function close_database($link) {
		try {
			mysqli_close($link);
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
?>