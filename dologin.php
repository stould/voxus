<?php
	require_once '/db_files/config.php';
	require_once '/db_files/database.php';
	require_once '/handlers/userHandler.php';
?>

<?php
	session_start();
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$email = trim($_POST["email"]);
		$password = trim($_POST["password"]);
			
		if(empty($email) || empty($password)){
			$error = "Fields must be filled.";
			header("location: loginform.php?login_error=".$error);
		}else{
			$link = open_database();
			$result = user_check_credentials($link, $email, $password);
			close_database($link);
			if($result == 0){
				$error = "Email and password does't match.";
				header("location: loginform.php?login_error=".$error);
			}else{
				$_SESSION["userlogin"] = $email;
				header("location: tasklist.php");
			}
		}
	}else{
		header("location: loginform.php");
	}

?>