<?php
	require_once '/handlers/loginHandler.php';
	
	require_once '/db_files/config.php';
	require_once '/db_files/database.php';
	require_once '/handlers/userHandler.php';
	require_once '/handlers/taskHandler.php';
?>

<?php

	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$code = trim($_POST["code"]);
		$taskname = trim($_POST["taskname"]);
		$description = trim($_POST["description"]);
		
		$message;
		$type;
		
				
		if(empty($code) || empty($taskname) || $_FILES["attachment"]["error"] != 0){
			$type = "register_taskerror";
			$message = "Fields must be filled.";
		}else{
			$fileName = $_FILES['attachment']['name'];
			$fileType = $_FILES['attachment']['type'];
			
			$fileSize = $_FILES['attachment']['size'];
			$fileTemp = $_FILES['attachment']['tmp_name'];
			
			$fp = fopen($fileTemp, "r");
			$fileContent = fread($fp, $fileSize);
			fclose($fp);
			
			$link = open_database();
			$result = task_insert($link, $code, $taskname, $description, $fileContent, $fileName, $fileType);
			
			close_database($link);
			if($result[0] == "error"){
				$type = "register_taskerror";
				$message = $result[1];
			}else{
				$type = "register_tasksuccess";
				$message = $result[1];
			}
		}
		header("location: taskform.php?$type=$message");
	}
?>