<?php
	require_once '/handlers/loginHandler.php';
	
	require_once '/db_files/config.php';
	require_once '/db_files/database.php';
	require_once '/handlers/taskHandler.php';
?>

<html>
	<head>
		<meta charset="UTF-8">
		<title>Updating task</title>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

		<style type="text/css">
			table {
				table-layout: fixed;
				word-wrap: break-word;
			}
			.wrapper{ width: 400px; padding: 20px; }
		</style>
	</head>
	<body>
	
		<?php include("header.php") ?>
	
		<div class="wrapper <?php if(ISSET($_GET['update_taskerror'])) echo 'has-error'; ?>">
			<h2>Updating task</h2>
			<?php 
				if(ISSET($_GET['update_taskerror'])){
					echo '<div class="alert alert-danger">'.$_GET["update_taskerror"].'</div>';
				}else if(ISSET($_GET['update_tasksuccess'])){
					echo '<div class="alert alert-success">'.$_GET["update_tasksuccess"].'</div>';
				}
				
				$link = open_database();
				$task = task_getByCode($link, $_GET["taskcode"])[1];
				close_database($link);
			?>
			<form action="updatetask.php" method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Code:</label>
					<input readonly="true" type="text" name="taskcode" class="form-control" value="<?php echo $task["code"]; ?>" >
				</div>    
				<div class="form-group">
					<label>Task name:</label>
					<input type="text" name="taskname" class="form-control" value="<?php echo $task["name"]; ?>">
				</div>
				<div class="form-group">
					<label>Description:</label>
					<input type="text" name="description" class="form-control" value="<?php echo $task["description"]; ?>">
				</div>  
				<div class="form-group">
					<label>Attachment:</label>
					<input type="file" name="attachment" class="btn btn-file" value="Register">
					<label>Current file:</label>
					<?php echo '<a class="" href="/download.php?taskCode='.$task["code"].'"> '.$task["filename"].'</a>'; ?>
					  <label title="Once selected, update will not change the current file in database."><input name="keepfile" type="checkbox"> Keep current file.</label>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Submit">
					<a href="/tasklist.php" id="cancel" name="cancel" class="btn btn-default">Cancel</a>
				</div>
			</form>
		</div>    
	</body>
</html>