<html>
	<head>
		<meta charset="UTF-8">
		<title>Register new user</title>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

		<style type="text/css">
			table {
				table-layout: fixed;
				word-wrap: break-word;
			}
			.wrapper{ width: 400px; padding: 20px; }
		</style>
		
	</head>
	<body>
		<div class="wrapper <?php if(ISSET($_GET['register_usererror'])) echo 'has-error'; ?>">
			<h2>Register new user</h2>
			<?php 
			if(ISSET($_GET['register_usererror'])){
				echo '<div class="alert alert-danger">'.$_GET["register_usererror"].'</div>';
			}else if(ISSET($_GET['register_usersuccess'])){
				echo '<div class="alert alert-success">'.$_GET["register_usersuccess"].'</div>';
			}
			?>
			<form action="registeruser.php" method="post">
				<div class="form-group">
					<label>Email:</label>
					<input type="text" name="email" class="form-control" value="">
				</div>    
				<div class="form-group">
					<label>Password:</label>
					<input type="password" name="password" class="form-control">
				</div>
				<div class="form-group">
					<label>Password confirmation:</label>
					<input type="password" name="password_confirm" class="form-control">
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success" value="Register">
					<a href="/loginform.php" id="cancel" name="cancel" class="btn btn-default">Cancel</a>
				</div>
			</form>
		</div>    
	</body>
</html>