<?php
	require_once '/handlers/loginHandler.php';

	require_once '/db_files/config.php';
	require_once '/db_files/database.php';
	require_once '/handlers/taskHandler.php';
?>
<?php
	$link = open_database();
	$file = task_getContent($link, $_GET["taskCode"]);
	close_database($link);
	task_downloadFile($file[1]);
?>